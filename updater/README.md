**Design updater**

การออกแบบระบบอัพเดทซอฟต์แวร์จำเป็นต้องคำนึงถึงความเสี่ยงหากเกิดปัญหาเมื่ออยู่ระหว่างการอัพเดทในอุปกรณ์ประเภท embedded system เพราะเกี่ยวข้องกับ reliability เมื่อเกิดปัญหาขึ้นอาจจะทำให้กลายเป็นปัญหาใหญ่ได้ 

**Resource**
- https://www.kernel.org/doc/ols/2005/ols2005v1-pages-21-36.pdf
- http://events.linuxfoundation.org/sites/events/files/slides/SoftwareUpdateForEmbedded.pdf
