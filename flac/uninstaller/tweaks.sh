#!/bin/sh
# some usefull things (thanks to oz_paulb from mazda3revolution.com)

get_cmu_sw_version()
{
	_ver=$(/bin/grep "^JCI_SW_VER=" /jci/version.ini | /bin/sed 's/^.*_\([^_]*\)\"$/\1/')
	_patch=$(/bin/grep "^JCI_SW_VER_PATCH=" /jci/version.ini | /bin/sed 's/^.*\"\([^\"]*\)\"$/\1/')
	_flavor=$(/bin/grep "^JCI_SW_FLAVOR=" /jci/version.ini | /bin/sed 's/^.*_\([^_]*\)\"$/\1/')

	if [ ! -z "${_flavor}" ]; then
		echo "${_ver}${_patch}-${_flavor}"
	else
		echo "${_ver}${_patch}"
	fi
}

log_message()
{
	echo "$*" 1>2
	echo "$*" >> "${MYDIR}/AIO_log.txt"
	/bin/fsync "${MYDIR}/AIO_log.txt"
}

show_message()
{
	sleep 5
	killall jci-dialog
#	log_message "= POPUP: $* "
	/jci/tools/jci-dialog --info --title="MESSAGE" --text="$*" --no-cancel &
}

remove_app_json()
# script by vic_bam85
{
	# check if app entry in additionalApps.json still exists, if so, then it will be deleted
	count=$(grep -c '{ "name": "'"${1}"'"' /jci/opera/opera_dir/userjs/additionalApps.json)
	if [ "$count" -gt "0" ]
		then
			log_message "=== ${count} entry(s) of ${1} found in additionalApps.json, app is already installed and will be deleted now ==="
			mv /jci/opera/opera_dir/userjs/additionalApps.json /jci/opera/opera_dir/userjs/additionalApps.json.old
			# delete last line with "]" from additionalApps.json
			grep -v "]" /jci/opera/opera_dir/userjs/additionalApps.json.old > /jci/opera/opera_dir/userjs/additionalApps.json
			sleep 2
			cp /jci/opera/opera_dir/userjs/additionalApps.json "${MYDIR}/additionalApps${1}-2._delete_last_line.json"
			# delete all app entrys from additionalApps.json
			sed -i "/${1}/d" /jci/opera/opera_dir/userjs/additionalApps.json
			sleep 2
			cp /jci/opera/opera_dir/userjs/additionalApps.json "${MYDIR}/additionalApps${1}-3._delete_app_entry.json"
			json="$(cat /jci/opera/opera_dir/userjs/additionalApps.json)"
			# check if last sign is comma
			rownend=$(echo -n "$json" | tail -c 1)
			if [ "$rownend" = "," ]
				then
					# if so, remove "," from back end
					echo "${json%,*}" > /jci/opera/opera_dir/userjs/additionalApps.json
					sleep 2
					log_message "=== Found comma at last line of additionalApps.json and deleted it ==="
			fi
			# add "]" again to last line of additionalApps.json
			echo "]" >> /jci/opera/opera_dir/userjs/additionalApps.json
			rm -f /jci/opera/opera_dir/userjs/additionalApps.json.old
			sleep 2
			cp /jci/opera/opera_dir/userjs/additionalApps.json "${MYDIR}/additionalApps${1}-4._after.json"
		else
			log_message "=== ${1} not found in additionalApps.json, no modification necessary ==="
	fi
}

# disable watchdog and allow write access
echo 1 > /sys/class/gpio/Watchdog\ Disable/value
mount -o rw,remount /

MYDIR=$(dirname "$(readlink -f "$0")")
CMU_SW_VER=$(get_cmu_sw_version)
rm -f "${MYDIR}/AIO_log.txt"

log_message "=== START LOGGING REMOVING... ==="
log_message "=== AIO 1.51  -  2016.09.26 ==="
log_message "=== CMU_SW_VER = ${CMU_SW_VER} ==="
log_message "=== MYDIR = ${MYDIR} ==="

# first test, if copy from MZD to sd card is working to test correct mount point
cp /jci/sm/sm.conf "${MYDIR}"
if [ -e "${MYDIR}/sm.conf" ]
	then
		log_message "=== Copytest to sd card successful, mount point is OK ==="
		log_message " "
		rm -f "${MYDIR}/sm.conf"
	else
		log_message "=== Copytest to sd card not successful, mount point not found! ==="
		/jci/tools/jci-dialog --title="ERROR!" --text="Mount point not found, have to reboot again" --ok-label='OK' --no-cancel &
		sleep 5
		reboot
		exit
fi

# Remove fps.js if still exists
if [ -e /jci/opera/opera_dir/userjs/fps.js ]
	then mv /jci/opera/opera_dir/userjs/fps.js /jci/opera/opera_dir/userjs/fps.js.org
	log_message "=== Moved /jci/opera/opera_dir/userjs/fps.js to fps.js.org  ==="
	log_message " "
fi

# a window will appear for 4 seconds to show the beginning of installation
show_message "START OF TWEAK DEINSTALLATION\n\n(This and the following message popup windows\n DO NOT have to be confirmed with OK)"


# remove track-order and FLAC support
show_message "REMOVE TRACK-ORDER AND FLAC SUPPORT ..."
log_message "REMOVE TRACK-ORDER AND FLAC SUPPORT ..."
if [ ${CMU_SW_VER} = "56.00.100A-ADR" ] \
|| [ ${CMU_SW_VER} = "56.00.240B-ADR" ] \
|| [ ${CMU_SW_VER} = "56.00.230A-EU" ] \
|| [ ${CMU_SW_VER} = "55.00.650A-NA" ] \
|| [ ${CMU_SW_VER} = "55.00.753A-NA" ] \
|| [ ${CMU_SW_VER} = "55.00.760A-NA" ]
	then
		trackorder_copy=1
		trackorder_copy_file=230A-EU
fi
if [ ${CMU_SW_VER} = "56.00.511A-EU" ] \
|| [ ${CMU_SW_VER} = "56.00.512A-EU" ] \
|| [ ${CMU_SW_VER} = "56.00.513B-EU" ] \
|| [ ${CMU_SW_VER} = "56.00.513C-EU" ] \
|| [ ${CMU_SW_VER} = "56.00.513C-ADR" ] \
|| [ ${CMU_SW_VER} = "58.00.250A-NA" ] \
|| [ ${CMU_SW_VER} = "56.00.521A-NA" ]
	then
		tackorder_copy=1
		tackorder_copy_file=511A-EU
fi

if [ ${CMU_SW_VER} = "59.00.326A-ADR" ] \
|| [ ${CMU_SW_VER} = "59.00.331A-EU" ]
	then
		trackorder_copy=1
		trackorder_copy_file=326A-ADR
fi

if test -s /jci/lib/libmc_user.so.org
	then
		rm -f /jci/lib/libmc_user.so
		mv /jci/lib/libmc_user.so.org /jci/lib/libmc_user.so
		log_message "=== Restored original /jci/lib/libmc_user.so from backup libmc_user.so.org ==="
	else
		if [ ${trackorder_copy} = "1" ]
			then
				cp -a ${MYDIR}/config_org/media-order-patching/jci/lib/libmc_user.so.${trackorder_copy_file} /jci/lib/libmc_user.so
				log_message "=== Detected ${CMU_SW_VER}: Copied fitting original /jci/lib/libmc_user.so ==="
				/bin/fsync /jci/lib/libmc_user.so
		fi
fi

if [ ${trackorder_copy} = "1" ]
	then
		for I in 1 2 3
		do
			status=`md5sum -c ${MYDIR}/config_org/media-order-patching/jci/lib/checksum.${trackorder_copy_file}.md5 2>&1 | grep FAILED`
			if [[ $? -ne 1 ]]
				then
					if [[ I -eq 3 ]]
						then
							log_message "=== Copying failed. Could not restore original library from backup! ==="
							break
						else
							log_message "=== ${I}. Checksum wrong of /jci/lib/libmc_user.so, try to copy libmc_user.so again from SD card ==="
							cp -a ${MYDIR}/config_org/media-order-patching/jci/lib/libmc_user.so.${trackorder_copy_file} /jci/lib/libmc_user.so
							sleep 3
							/bin/fsync /jci/lib/libmc_user.so
						continue
					fi
				else
					log_message "=== Checksum OK of /jci/lib/libmc_user.so, copy was succesful ==="
					break
			fi
		done
fi

chmod 755 /jci/lib/libmc_user.so
rm -f /usr/lib/gstreamer-0.10/libgstflac.so
rm -f /usr/lib/libFLAC.so.8.3.0
rm -f /usr/lib/libFLAC.so.8
log_message "END DEINSTALLATION OF TRACK-ORDER AND FLAC SUPPORT"
log_message " "


log_message " "
log_message "END OF TWEAKS DEINSTALLATION"

# a window will appear for asking to reboot automatically
sleep 3
killall jci-dialog
/jci/tools/jci-dialog --info --title="REMOVED SELECTED AIO TWEAKS" --text="REBOOTING IN A FEW SECONDS!" --no-cancel &
sleep 10
killall jci-dialog
reboot
