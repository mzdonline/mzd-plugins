#!/bin/sh
# some usefull things (thanks to oz_paulb from mazda3revolution.com)

get_cmu_sw_version()
{
	_ver=$(/bin/grep "^JCI_SW_VER=" /jci/version.ini | /bin/sed 's/^.*_\([^_]*\)\"$/\1/')
	_patch=$(/bin/grep "^JCI_SW_VER_PATCH=" /jci/version.ini | /bin/sed 's/^.*\"\([^\"]*\)\"$/\1/')
	_flavor=$(/bin/grep "^JCI_SW_FLAVOR=" /jci/version.ini | /bin/sed 's/^.*_\([^_]*\)\"$/\1/')

	if [ ! -z "${_flavor}" ]; then
		echo "${_ver}${_patch}-${_flavor}"
	else
		echo "${_ver}${_patch}"
	fi
}

log_message()
{
	echo "$*" 1>2
	echo "$*" >> "${MYDIR}/AIO_log.txt"
	/bin/fsync "${MYDIR}/AIO_log.txt"
}

show_message()
{
	sleep 5
	killall jci-dialog
#	log_message "= POPUP: $* "
	/jci/tools/jci-dialog --info --title="MESSAGE" --text="$*" --no-cancel &
}

show_message_OK()
{
	sleep 4
	killall jci-dialog
#	log_message "= POPUP: $* "
	/jci/tools/jci-dialog --confirm --title="CONTINUE INSTALLATION?" --text="$*" --ok-label="YES - GO ON" --cancel-label="NO - ABORT"
	if [ $? != 1 ]
		then
			killall jci-dialog
			return
		else
			show_message "INSTALLATION ABORTED! PLEASE UNPLUG USB DRIVE"
			sleep 5
			exit
		fi
}

add_app_json()
# script by vic_bam85
{	
	# check if entry in additionalApps.json still exists, if so nothing is to do
	count=$(grep -c '{ "name": "'"${1}"'"' /jci/opera/opera_dir/userjs/additionalApps.json)
	if [ "$count" = "0" ]
		then
			log_message "=== No entry of ${2} found in additionalApps.json, seems to be the first installation ==="
			mv /jci/opera/opera_dir/userjs/additionalApps.json /jci/opera/opera_dir/userjs/additionalApps.json.old
			sleep 2
			# delete last line with "]" from additionalApps.json
			grep -v "]" /jci/opera/opera_dir/userjs/additionalApps.json.old > /jci/opera/opera_dir/userjs/additionalApps.json
			sleep 2
			cp /jci/opera/opera_dir/userjs/additionalApps.json "${MYDIR}/additionalApps${1}-2._delete_last_line.json"
			# check, if other entrys exists
			count=$(grep -c '}' /jci/opera/opera_dir/userjs/additionalApps.json)
			if [ "$count" != "0" ]
				then
					# if so, add "," to the end of last line to additionalApps.json
					echo "$(cat /jci/opera/opera_dir/userjs/additionalApps.json)", > /jci/opera/opera_dir/userjs/additionalApps.json
					sleep 2
					cp /jci/opera/opera_dir/userjs/additionalApps.json "${MYDIR}/additionalApps${1}-3._add_comma_to_last_line.json"
					log_message "=== Found existing entrys in additionalApps.json ==="
			fi
			# add app entry and "]" again to last line of additionalApps.json
			log_message "=== Add ${2} to last line of additionalApps.json ==="
			echo '{ "name": "'"${1}"'", "label": "'"${2}"'" }' >> /jci/opera/opera_dir/userjs/additionalApps.json
			sleep 2
			cp /jci/opera/opera_dir/userjs/additionalApps.json "${MYDIR}/additionalApps${1}-4._add_entry_to_last_line.json"
			echo "]" >> /jci/opera/opera_dir/userjs/additionalApps.json
			sleep 2
			cp /jci/opera/opera_dir/userjs/additionalApps.json "${MYDIR}/additionalApps${1}-5._after.json"
			rm -f /jci/opera/opera_dir/userjs/additionalApps.json.old
		else
			log_message "=== ${2} already exists in additionalApps.json, no modification necessary ==="
	fi
}

# disable watchdog and allow write access
echo 1 > /sys/class/gpio/Watchdog\ Disable/value
mount -o rw,remount /

MYDIR=$(dirname "$(readlink -f "$0")")
CMU_SW_VER=$(get_cmu_sw_version)
rm -f "${MYDIR}/AIO_log.txt"

log_message "=== START LOGGING INSTALLING... ==="
log_message "=== AIO 1.51  -  2016.09.26 ==="
log_message "=== CMU_SW_VER = ${CMU_SW_VER} ==="
log_message "=== MYDIR = ${MYDIR} ==="

# first test, if copy from MZD to sd card is working to test correct mount point
cp /jci/sm/sm.conf "${MYDIR}"
if [ -e "${MYDIR}/sm.conf" ]
	then
		log_message "=== Copytest to sd card successful, mount point is OK ==="
		log_message " "
		rm -f "${MYDIR}/sm.conf"
	else
		log_message "=== Copytest to sd card not successful, mount point not found! ==="
		/jci/tools/jci-dialog --title="ERROR!" --text="Mount point not found, have to reboot again" --ok-label='OK' --no-cancel &
		sleep 5
		reboot
		exit
fi

if [ "${CMU_SW_VER}" = "56.00.100A-ADR" ] \
|| [ "${CMU_SW_VER}" = "56.00.230A-ADR" ] \
|| [ "${CMU_SW_VER}" = "56.00.240B-ADR" ] \
|| [ "${CMU_SW_VER}" = "56.00.511A-ADR" ] \
|| [ "${CMU_SW_VER}" = "56.00.512A-ADR" ] \
|| [ "${CMU_SW_VER}" = "56.00.513C-ADR" ] \
|| [ "${CMU_SW_VER}" = "59.00.326A-ADR" ] \
|| [ "${CMU_SW_VER}" = "56.00.230A-EU" ] \
|| [ "${CMU_SW_VER}" = "56.00.511A-EU" ] \
|| [ "${CMU_SW_VER}" = "56.00.512A-EU" ] \
|| [ "${CMU_SW_VER}" = "56.00.513B-EU" ] \
|| [ "${CMU_SW_VER}" = "56.00.513C-EU" ] \
|| [ "${CMU_SW_VER}" = "59.00.331A-EU" ] \
|| [ "${CMU_SW_VER}" = "55.00.650A-NA" ] \
|| [ "${CMU_SW_VER}" = "55.00.753A-NA" ] \
|| [ "${CMU_SW_VER}" = "55.00.760A-NA" ] \
|| [ "${CMU_SW_VER}" = "56.00.521A-NA" ] \
|| [ "${CMU_SW_VER}" = "58.00.250A-NA" ] \
|| [ "${CMU_SW_VER}" = "56.00.401A-JP" ]
	then
		show_message_OK "Detected compatible version ${CMU_SW_VER}\n\n To continue installation choose YES\n To abort choose NO"
		log_message "Detected compatible version ${CMU_SW_VER}"
	else
		show_message_OK "Detected previously unknown version ${CMU_SW_VER}!\n\n To continue anyway choose YES\n To abort choose NO"
		log_message "Detected previously unknown version ${CMU_SW_VER}!"
fi

# a window will appear for 4 seconds to show the beginning of installation
show_message "START OF TWEAK INSTALLATION\n\n(This and the following message popup windows\n DO NOT have to be confirmed with OK)"
log_message " "

# disable watchdogs in /jci/sm/sm.conf to avoid boot loops if somthing goes wrong
if [ ! -e /jci/sm/sm.conf.org ]
	then
		cp -a /jci/sm/sm.conf /jci/sm/sm.conf.org
		log_message "=== Backup of /jci/sm/sm.conf to sm.conf.org ==="
	else log_message "=== Backup of /jci/sm.conf.org already there! ==="
fi
sed -i 's/watchdog_enable="true"/watchdog_enable="false"/g' /jci/sm/sm.conf
sed -i 's|args="-u /jci/gui/index.html"|args="-u /jci/gui/index.html --noWatchdogs"|g' /jci/sm/sm.conf
log_message "=== WATCHDOG IN SM.CONF PERMANENTLY DISABLED ==="

# -- Enable userjs and allow file XMLHttpRequest in /jci/opera/opera_home/opera.ini - backup first - then edit
if [ ! -e /jci/opera/opera_home/opera.ini.org ]
	then
		cp -a /jci/opera/opera_home/opera.ini /jci/opera/opera_home/opera.ini.org
		log_message "=== Backup of /jci/opera/opera_home/opera.ini to opera.ini.org ==="
	else log_message "=== Backup of /jci/opera/opera_home/opera.ini.org already there! ==="
fi
sed -i 's/User JavaScript=0/User JavaScript=1/g' /jci/opera/opera_home/opera.ini
count=$(grep -c "Allow File XMLHttpRequest=" /jci/opera/opera_home/opera.ini)
if [ "$count" = "0" ]
	then
		sed -i '/User JavaScript=.*/a Allow File XMLHttpRequest=1' /jci/opera/opera_home/opera.ini
	else
		sed -i 's/Allow File XMLHttpRequest=.*/Allow File XMLHttpRequest=1/g' /jci/opera/opera_home/opera.ini
fi
log_message "=== ENABLED USERJS AND ALLOWED FILE XMLHTTPREQUEST IN /JCI/OPERA/OPERA_HOME/OPERA.INI  ==="
log_message " "

# Remove fps.js if still exists
if [ -e /jci/opera/opera_dir/userjs/fps.js ]
	then mv /jci/opera/opera_dir/userjs/fps.js /jci/opera/opera_dir/userjs/fps.js.org
	log_message "=== Moved /jci/opera/opera_dir/userjs/fps.js to fps.js.org  ==="
	log_message " "
fi

# Enable WIFI
if grep -Fq "(framework.localize.REGIONS['NorthAmerica'])" /jci/gui/apps/syssettings/js/syssettingsApp.js
	then
		cp /jci/gui/apps/syssettings/js/syssettingsApp.js "${MYDIR}/syssettingsApp_WIFI-before.js"
		LN=$(grep -n "if((region != (framework.localize.REGIONS\['NorthAmerica'\]) [&|]\{2\} (region != (framework.localize.REGIONS\['Japan'\]))))" /jci/gui/apps/syssettings/js/syssettingsApp.js | cut -d":" -f1)
		if [ ! -z "$LN" ]; then
			sed -i "$LN"'s/NorthAmerica/Japan/' /jci/gui/apps/syssettings/js/syssettingsApp.js
			log_message "=== Wifi for NA enabled ==="
		else
			log_message "=== ERROR: Line For Wifi Fix Not Found ==="
		fi
		cp /jci/gui/apps/syssettings/js/syssettingsApp.js "${MYDIR}/syssettingsApp_WIFI-after.js"
	else
		echo "exist"
		log_message "=== Wifi already active ==="
		log_message " "
fi


# track-order and FLAC support
show_message "INSTALL TRACK-ORDER AND FLAC SUPPORT ..."
log_message "INSTALL TRACK-ORDER AND FLAC SUPPORT ..."

if [ ${CMU_SW_VER} = "56.00.100A-ADR" ] \
|| [ ${CMU_SW_VER} = "56.00.240B-ADR" ] \
|| [ ${CMU_SW_VER} = "56.00.230A-EU" ] \
|| [ ${CMU_SW_VER} = "55.00.650A-NA" ] \
|| [ ${CMU_SW_VER} = "55.00.753A-NA" ] \
|| [ ${CMU_SW_VER} = "55.00.760A-NA" ]
	then
		trackorder_copy=1
		trackorder_copy_file=230A-EU
fi

if [ ${CMU_SW_VER} = "56.00.511A-EU" ] \
|| [ ${CMU_SW_VER} = "56.00.512A-EU" ] \
|| [ ${CMU_SW_VER} = "56.00.513B-EU" ] \
|| [ ${CMU_SW_VER} = "56.00.513C-EU" ] \
|| [ ${CMU_SW_VER} = "56.00.513C-ADR" ] \
|| [ ${CMU_SW_VER} = "58.00.250A-NA" ] \
|| [ ${CMU_SW_VER} = "56.00.521A-NA" ]
	then
		trackorder_copy=1
		trackorder_copy_file=511A-EU
fi

if [ ${CMU_SW_VER} = "59.00.326A-ADR" ] \
|| [ ${CMU_SW_VER} = "59.00.331A-EU" ]
	then
		trackorder_copy=1
		trackorder_copy_file=326A-ADR
fi

if [ ! -e /jci/lib/libmc_user.so.org ]
	then
		cp -a /jci/lib/libmc_user.so /jci/lib/libmc_user.so.org
		log_message "=== Backup of /jci/lib/libmc_user.so to libmc_user.so.org ==="
fi

if [ ${trackorder_copy} = "1" ]
		then
			cp -a ${MYDIR}/config/media-order-patching/jci/lib/libmc_user.so.${trackorder_copy_file} /jci/lib/libmc_user.so
			cp -a ${MYDIR}/config/media-order-patching/usr/lib/gstreamer-0.10/libgstflac.so /usr/lib/gstreamer-0.10
			cp -a ${MYDIR}/config/media-order-patching/usr/lib/libFLAC.so.8.3.0 /usr/lib
			log_message "=== Detected ${CMU_SW_VER}: Copied fitting patched /jci/lib/libmc_user.so ==="
			/bin/fsync /jci/lib/libmc_user.so
fi

if [ ${trackorder_copy} = "1" ]
	then
		for I in 1 2 3
		do
			status=`md5sum -c ${MYDIR}/config/media-order-patching/jci/lib/checksum.${trackorder_copy_file}.md5 2>&1 | grep FAILED`
			if [[ $? -ne 1 ]]
				then
					if [[ I -eq 3 ]]
						then
							log_message "=== Copying failed. Restoring original library from backup! ==="
							mv /jci/lib/libmc_user.so.org /jci/lib/libmc_user.so
							break
						else
							log_message "=== ${I}. Checksum wrong of /jci/lib/libmc_user.so, try to copy libmc_user.so again from SD card ==="
							cp -a ${MYDIR}/config/media-order-patching/jci/lib/libmc_user.so.${trackorder_copy_file} /jci/lib/libmc_user.so
							sleep 3
							/bin/fsync /jci/lib/libmc_user.so
						continue
					fi
				else
					log_message "=== Checksum OK of /jci/lib/libmc_user.so, copy was succesful ==="
					break
			fi
		done
fi

chmod 755 /jci/lib/libmc_user.so
chmod 755 /usr/lib/gstreamer-0.10/libgstflac.so
chmod 755 /usr/lib/libFLAC.so.8.3.0
ln -s /usr/lib/libFLAC.so.8.3.0 /usr/lib/libFLAC.so.8
log_message "END INSTALLATION OF TRACK-ORDER AND FLAC SUPPORT"
log_message " "
log_message " "
sleep 2
log_message "END OF TWEAKS INSTALLATION"

# a window will appear for asking to reboot automatically
sleep 3
killall jci-dialog
sleep 3
/jci/tools/jci-dialog --confirm --title="SELECTED AIO TWEAKS APPLIED" --text="Click OK to reboot the system"
		if [ $? != 1 ]
		then
			sleep 10
			reboot
			exit
		fi
sleep 2
killall jci-dialog
