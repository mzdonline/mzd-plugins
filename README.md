# !WARNING! "No Longer Maintained". Do not use.

## mzd-plugins
เรา(@mzdonline)ไม่ใช่เจ้าของโค๊ด เป็นเพียงผู้ที่รวบรวมไฟล์ ไฟล์ส่วนใหญ่ได้จาก [AIO](https://github.com/Siutsch) ความดีทั้งหมดขอมอบให้ [spadival](https://github.com/spadival), [Siutsch](https://github.com/Siutsch), [gartnera](https://github.com/gartnera/headunit), [borconi](https://github.com/borconi), [lmagder](https://github.com/lmagder) และนักพัฒนาที่ไม่ได้กล่าวถึงจาก mazda3revolution.com 
## คำอธิบาย
- [kmL](kmL/README.md) : แสดงผล L/100km และ km/L ในเมนู FuelConsumption (เวอร์ชั่นคุณ [@Pentium4](http://www.allnewmazda3.com/index.php?topic=4773.0))
-  <del>VideoPlayer3 : Native Opera Video Player (HTML5)</del> ยังใช้งานไม่ได้
- dump1090 : โปรแกรมและไดร์เวอร์สำหรับ RTL-SDR เพื่อใช้กับ CMU เป็นคำสั่ง cli สั่งงานผ่านหน้าจอไม่ได้
- headunit : ย้ายไป https://github.com/mzdonline/AR4

## Directory description
 - We are not own the code. Just only compiled. Thank you for hard work from [spadival](https://github.com/spadival), [Siutsch](https://github.com/Siutsch), [gartnera](https://github.com/gartnera/headunit), [borconi](https://github.com/borconi), [lmagder](https://github.com/lmagder). and developers from mazda3revolution.com.
 - [kmL](kmL/README.md) : Display L/100km and km/L in FuelConsumption application (version [@Pentium4](http://www.allnewmazda3.com/index.php?topic=4773.0))
 - <del>VideoPlayer3 : Native Opera Video Player (HTML5) credit : inventor [@vic_bam85](http://mazda3revolution.com/forums/1709434-post765.html) </del>. Sorry for bug : uninstall script should post with in a week.
 - dump1090 support RTL-SDR to decode ADS-B 
 - headunit : move to https://github.com/mzdonline/AR4
