#!/bin/sh

# some usefull things (thanks to oz_paulb from mazda3revolution.com - your code is awesome! I wish, I could understand everything ...)

get_cmu_sw_version()
{
	_ver=$(/bin/grep "^JCI_SW_VER=" /jci/version.ini | /bin/sed 's/^.*_\([^_]*\)\"$/\1/')
	_patch=$(/bin/grep "^JCI_SW_VER_PATCH=" /jci/version.ini | /bin/sed 's/^.*\"\([^\"]*\)\"$/\1/')
	_flavor=$(/bin/grep "^JCI_SW_FLAVOR=" /jci/version.ini | /bin/sed 's/^.*_\([^_]*\)\"$/\1/')

	if [[ ! -z "${_flavor}" ]]; then
		echo "${_ver}${_patch}-${_flavor}"
	else
		echo "${_ver}${_patch}"
	fi
}

get_cmu_sw_version_only()
{
	_veronly=$(/bin/grep "^JCI_SW_VER=" /jci/version.ini | /bin/sed 's/^.*_\([^_]*\)\"$/\1/')
	echo "${_veronly}"
}

log_message()
{
	echo "$*" 1>2
	echo "$*" >> "${MYDIR}/AIO_log.txt"
	/bin/fsync "${MYDIR}/AIO_log.txt"
}


show_message()
{
	sleep 4
	killall jci-dialog
	log_message "= POPUP: $* "
	/jci/tools/jci-dialog --info --title="MESSAGE" --text="$*" --no-cancel &
}


show_message_OK()
{
	sleep 4
	killall jci-dialog
	log_message "= POPUP: $* "
	/jci/tools/jci-dialog --confirm --title="CONTINUE UNINSTALLATION?" --text="$*" --ok-label="YES - GO ON" --cancel-label="NO - ABORT"
	if [ $? != 1 ]
		then
			killall jci-dialog
			break
		else
			show_message "UNINSTALLATION ABORTED! PLEASE UNPLUG USB DRIVE"
			sleep 5
			exit
		fi
}

# disable watchdog and allow write access
echo 1 > /sys/class/gpio/Watchdog\ Disable/value
mount -o rw,remount /

MYDIR=$(dirname $(readlink -f $0))
CMU_SW_VER=$(get_cmu_sw_version)
CMU_VER_ONLY=$(get_cmu_sw_version_only)
rm -f "${MYDIR}/AIO_log.txt"

log_message "=== START LOGGING ... ==="
log_message "=== BACKUP CMU_SW_VER = ${CMU_SW_VER} ==="
log_message "=== MYDIR = ${MYDIR} ==="

if [ -e  /jci/gui/apps/ecoenergy/controls/FuelConsumption.kmL ]
  then
   rm -rf /jci/gui/apps/ecoenergy/controls/FuelConsumption/css
   rm -rf /jci/gui/apps/ecoenergy/controls/FuelConsumption/js
   
   cp -a /jci/gui/apps/ecoenergy/controls/FuelConsumption.kmL/css /jci/gui/apps/ecoenergy/controls/FuelConsumption
   cp -a /jci/gui/apps/ecoenergy/controls/FuelConsumption.kmL/js /jci/gui/apps/ecoenergy/controls/FuelConsumption
   chown -R `ls -l /jci/gui/apps/ecoenergy/controls|grep EcoEffect|awk '{print $3}'`:`ls -l /jci/gui/apps/ecoenergy/controls|grep EcoEffect|awk '{print $4}'` /jci/gui/apps/ecoenergy/controls/FuelConsumption
   log_message "=== END UNINSTALLATION OF Display km/L ==="
  else 
   log_message "== Not Found backup on your USB =="
fi

# a window will appear for asking to reboot automatically
sleep 4
killall jci-dialog
sleep 3
/jci/tools/jci-dialog --confirm --title="SELECTED ALL-IN-ONE TWEAKS APPLIED" --text="Click OK to reboot the system"
		if [ $? != 1 ]
		then
			reboot
			exit
		fi
sleep 10
killall jci-dialog
