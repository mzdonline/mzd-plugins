#!/bin/sh

# some usefull things (thanks to oz_paulb from mazda3revolution.com - your code is awesome! I wish, I could understand everything ...)

get_cmu_sw_version()
{
	_ver=$(/bin/grep "^JCI_SW_VER=" /jci/version.ini | /bin/sed 's/^.*_\([^_]*\)\"$/\1/')
	_patch=$(/bin/grep "^JCI_SW_VER_PATCH=" /jci/version.ini | /bin/sed 's/^.*\"\([^\"]*\)\"$/\1/')
	_flavor=$(/bin/grep "^JCI_SW_FLAVOR=" /jci/version.ini | /bin/sed 's/^.*_\([^_]*\)\"$/\1/')

	if [[ ! -z "${_flavor}" ]]; then
		echo "${_ver}${_patch}-${_flavor}"
	else
		echo "${_ver}${_patch}"
	fi
}

get_cmu_sw_version_only()
{
	_veronly=$(/bin/grep "^JCI_SW_VER=" /jci/version.ini | /bin/sed 's/^.*_\([^_]*\)\"$/\1/')
	echo "${_veronly}"
}

log_message()
{
	echo "$*" 1>2
	echo "$*" >> "${MYDIR}/AIO_log.txt"
	/bin/fsync "${MYDIR}/AIO_log.txt"
}


show_message()
{
	sleep 4
	killall jci-dialog
	log_message "= POPUP: $* "
	/jci/tools/jci-dialog --info --title="MESSAGE" --text="$*" --no-cancel &
}


show_message_OK()
{
	sleep 4
	killall jci-dialog
	log_message "= POPUP: $* "
	/jci/tools/jci-dialog --confirm --title="CONTINUE INSTALLATION?" --text="$*" --ok-label="YES - GO ON" --cancel-label="NO - ABORT"
	if [ $? != 1 ]
		then
			killall jci-dialog
			break
		else
			show_message "INSTALLATION ABORTED! PLEASE UNPLUG USB DRIVE"
			sleep 5
			exit
		fi
}

# disable watchdog and allow write access
echo 1 > /sys/class/gpio/Watchdog\ Disable/value
mount -o rw,remount /

MYDIR=$(dirname $(readlink -f $0))
CMU_SW_VER=$(get_cmu_sw_version)
CMU_VER_ONLY=$(get_cmu_sw_version_only)
rm -f "${MYDIR}/AIO_log.txt"

log_message "=== START LOGGING ... ==="
# log_message "=== CMU_SW_VER = ${CMU_SW_VER} ==="
log_message "=== MYDIR = ${MYDIR} ==="
log_message "=== Watchdog temporary disabeld and write access enabled ==="

# first test, if copy from MZD to sd card is working to test correct mount point
cp /jci/sm/sm.conf ${MYDIR}/config
if [ -e ${MYDIR}/config/sm.conf ]
	then
		log_message "=== Copytest to sd card successful, mount point is OK ==="
		rm -f ${MYDIR}/config/sm.conf
	else
		log_message "=== Copytest to sd card not successful, mount point not found! ==="
		/jci/tools/jci-dialog --title="ERROR!" --text="Mount point not found, have to reboot again" --ok-label='OK' --no-cancel &
		sleep 5
		reboot
		exit
fi

show_message_OK "Version = ${CMU_SW_VER} : To continue installation press OK"

# a window will appear for 4 seconds to show the beginning of installation
show_message "START OF TWEAK INSTALLATION ..."

# disable watchdogs in /jci/sm/sm.conf to avoid boot loops if somthing goes wrong
if [ ! -e /jci/sm/sm.conf.kmL ]
	then
            #compatible AIO
            if [ -e /jci/sm/sm.conf.org ] 
             then
		cp -a /jci/sm/sm.conf.org /jci/sm/sm.conf.kmL
              else
		cp -a /jci/sm/sm.conf /jci/sm/sm.conf.kmL
             fi
		log_message "=== Backup of /jci/sm/sm.conf to sm.conf.kmL ==="
	else log_message "=== Backup of /jci/sm.conf.kmL already there! ==="
fi
sed -i 's/watchdog_enable="true"/watchdog_enable="false"/g' /jci/sm/sm.conf
sed -i 's|args="-u /jci/gui/index.html"|args="-u /jci/gui/index.html --noWatchdogs"|g' /jci/sm/sm.conf

log_message "=== WATCHDOG IN SM.CONF PERMANENTLY DISABLED ==="

# INSTALL Display km/L
show_message "INSTALL Display km/L ..."
if [ ! -e  /jci/gui/apps/ecoenergy/controls/FuelConsumption.kmL ]
 then
   #backup original
   cp -a /jci/gui/apps/ecoenergy/controls/FuelConsumption /jci/gui/apps/ecoenergy/controls/FuelConsumption.kmL
fi
rm -rf /jci/gui/apps/ecoenergy/controls/FuelConsumption/css
rm -rf /jci/gui/apps/ecoenergy/controls/FuelConsumption/js
cp -a ${MYDIR}/config/consumption-kmL/FuelConsumption-patch/css /jci/gui/apps/ecoenergy/controls/FuelConsumption/
cp -a ${MYDIR}/config/consumption-kmL/FuelConsumption-patch/js /jci/gui/apps/ecoenergy/controls/FuelConsumption/

#Set same origin owner
chown -R `ls -l /jci/gui/apps/ecoenergy/controls|grep EcoEffect|awk '{print $3}'`:`ls -l /jci/gui/apps/ecoenergy/controls|grep EcoEffect|awk '{print $4}'` /jci/gui/apps/ecoenergy/controls/FuelConsumption

log_message "=== END INSTALLATION OF Display km/L ==="

# a window will appear for asking to reboot automatically
sleep 4
killall jci-dialog
sleep 3
/jci/tools/jci-dialog --confirm --title="SELECTED ALL-IN-ONE TWEAKS APPLIED" --text="Click OK to reboot the system"
		if [ $? != 1 ]
		then
			reboot
			exit
		fi
sleep 10
killall jci-dialog
